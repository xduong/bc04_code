import React, { useEffect, useLayoutEffect } from "react";
import { localServ } from "../services/localService";

export default function SecureView({ children }) {
  useEffect(() => {
    let userLocal = localServ.user.get();
    if (!userLocal) {
      window.location.href = "/login";
    }
  }, []);
  // useLayoutEffect(() => {
  //   let userLocal = localServ.user.get();
  //   if (!userLocal) {
  //     window.location.href = "/login";
  //   }
  // }, []);

  return <div>{children}</div>;
}
