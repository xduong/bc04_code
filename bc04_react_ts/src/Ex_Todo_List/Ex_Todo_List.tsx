import React, { useState } from "react";
import { InterfaceTodo } from "./Interface/interface_Ex_TodoList";
import TodoForm from "./TodoForm/TodoForm";
import TodoList from "./TodoList/TodoList";

type Props = {};

export default function Ex_Todo_List({}: Props) {
  const [todos, setTodos] = useState<InterfaceTodo[]>([
    {
      id: "1",
      title: "Làm capstone movie",
      isCompleted: false,
    },
    {
      id: "2",
      title: "Làm dự án cuối khoá",
      isCompleted: false,
    },
  ]);
  const handleAddTodo = (todo: InterfaceTodo) => {
    let newTodos = [...todos, todo];
    setTodos(newTodos);
  };

  const handleTodoRemove = (idTodo: string) => {
    let newTodos = todos.filter((item) => {
      return item.id !== idTodo;
    });
    setTodos(newTodos);
  };
  return (
    <div className="container  py-5">
      <TodoForm handleAddTodo={handleAddTodo} />
      <TodoList handleTodoRemove={handleTodoRemove} todos={todos} />
    </div>
  );
}
