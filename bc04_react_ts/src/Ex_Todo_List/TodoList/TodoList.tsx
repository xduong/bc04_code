import React from "react";
import { InterfaceTodoListComponent } from "../Interface/interface_Ex_TodoList";
import TodoItem from "../TodoItem/TodoItem";

type Props = {};

export default function TodoList({
  todos,
  handleTodoRemove,
}: InterfaceTodoListComponent) {
  return (
    <table className="table">
      <thead>
        <tr>
          <th>ID</th>
          <th>Title</th>
          <th>Is completed</th>
        </tr>
      </thead>
      <tbody>
        {todos.map((todo) => {
          return <TodoItem handleTodoRemove={handleTodoRemove} todo={todo} />;
        })}
      </tbody>
    </table>
  );
}
