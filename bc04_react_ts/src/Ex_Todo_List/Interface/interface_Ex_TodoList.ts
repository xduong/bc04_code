export interface InterfaceTodo {
  id: string;
  title: string;
  isCompleted: boolean;
}
export interface InterfaceTodoListComponent {
  todos: InterfaceTodo[];
  handleTodoRemove: (id: string) => void;
}

export interface InterfaceTodoItemComponent {
  todo: InterfaceTodo;
  handleTodoRemove: (id: string) => void;

  //
  //
}

export interface InterfaceTodoFormComponent {
  handleAddTodo: (value: InterfaceTodo) => void;
}
