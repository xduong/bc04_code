import React from "react";
import { InterfaceTodoItemComponent } from "../Interface/interface_Ex_TodoList";

type Props = {};

export default function TodoItem({
  todo,
  handleTodoRemove,
}: InterfaceTodoItemComponent) {
  return (
    <tr>
      <td>{todo.id}</td>
      <td>{todo.title}</td>
      <td>
        <input type="checkbox" checked={todo.isCompleted} />
        <button
          onClick={() => {
            handleTodoRemove(todo.id);
          }}
          className="btn btn-danger ml-3"
        >
          Delete
        </button>
      </td>
    </tr>
  );
}
