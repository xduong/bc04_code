import React from "react";
import UserInfor from "./UserInfor";

type Props = {};
export interface UserData {
  name: string;
  age: number;
}
let data: UserData = {
  name: "aclice",
  age: 2,
};

export default function DemoProps({}: Props) {
  return (
    <div>
      <UserInfor user={data} />
    </div>
  );
}
// tsrfc
