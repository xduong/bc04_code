import React from "react";
import { UserData } from "./DemoProps";

interface UserProps {
  user: UserData;
}

export default function UserInfor({ user }: UserProps) {
  return (
    <div className="alert-primary">
      <h2>Name: {user.name}</h2>
      <h2>Age: {user.age}</h2>
    </div>
  );
}

// tsrfc
