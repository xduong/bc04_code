import SecureView from "HOC/SecureView";
import LoginPage from "pages/LoginPage/LoginPage";
import UserManagementPage from "pages/UserManagementPage/UserManagementPage";

export const routes = [
  {
    path: "/login",
    component: <LoginPage />,
  },
  {
    path: "/user-management",
    component: <SecureView Component={UserManagementPage} />,
  },
  {
    path: "/",
    component: <SecureView Component={UserManagementPage} />,
  },
];
